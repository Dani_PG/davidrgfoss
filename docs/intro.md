---
sidebar_position: 1
---

# Manuales Introducción

Este es el apartado de documentación. En este apartado veremos agrupados distintas cosas:
 
* Manuales individuales: Cosas puntuales como configurar un servicio o servidor para algo concreto.
* Manuales agrupados: Manuales que agrupan distintas cosas que conformen una tarea final. Por ejemplo montar un escenario con distintos servicios.
* Explicación de códigos: Publicaré también como usar un comando concreto, o explicar algún código que yo haya creado.


A la izquierda tendremos un menú que nos permitirá escoger sobre que queremos leer. 
