---
slug: Bienvenidos!
title: Bienvenidos!
authors: David
tags: [davidrgfoss, bienvenida, anuncios]
---

Prueba1 Prueba2

¡Bienvenidos a todos!

Antes que nada me encantaría poder presentarme, yo soy David. Siempre he tenido la informática como pasión en la vida, me encanta aprender cosas nuevas de forma constante, pero es casi imposible conocer todo en este mundo de la «informática».

Esto es un proyecto pensado para poder compartir mi conocimiento en distintos manuales, códigos o proyectos. Todo será sin coste alguno para el usuario, soy un gran partidario de la **filosofía _Open-Source_ y del conocimiento totalmente gratuito**. Aunque veremos también manuales o artículos enfocados al uso de software privado, porque hay que tener claro que ambos mundos tienen cosas que aportar y cosas que mejorar. 

Este proyecto es un rediseño de un antiguo blog mío el cual sigue estando disponible en [Dram Blog](http://dram.byethost9.com). También ha sido posible gracias a la empresa «**QuantCDN**» que me ha permitido alojar esta web estática de forma gratuita, así como al proyecto «**Docusaurus**».

¡Estoy encantado de poder daros la bienvenida a todos los usuarios que entréis en esta web, espero que podáis disfrutarla y aprender cosas nuevas de mí!

![Paisaje Bonito](./banner23.jpg)
