// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Davidrg FOSS',
  tagline: 'En este sitio encontraras diversos manuales o codigos desarrollados por mi.',
  url: 'https://www.davidrgfoss.com',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'gitlab/davidrg', // Usually your GitHub org/user name.
  projectName: 'Davidrg FOSS', // Usually your repo name.

  presets: [
    [
      '@docusaurus/preset-classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl: 'https://www.gitlab.com/davidrg/davidrgfoss/-/blob/main/',
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/davidrg/davidrgfoss/-/blob/main/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      algolia: {
      apiKey: 'cb1b09a92a7183486fca27e12e6accea',
      indexName: 'davidrgfoss',

      // Optional: see doc section below
      contextualSearch: true,

      // Optional: see doc section below
      appId: '2O1H21K8KT',

      // Optional: Algolia search parameters
      searchParameters: {},

      //... other Algolia params
    },
      navbar: {
        title: 'Davidrg FOSS',
        logo: {
          alt: 'Pagina web Davidrg FOSS',
          src: 'img/logo.svg',
        },
        items: [
          {
            type: 'doc',
            docId: 'intro',
            position: 'left',
            label: 'Manuales',
          },
          {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://gitlab.com/davidrg',
            label: 'GitLab',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Documentación',
            items: [
              {
                label: 'Manuales',
                to: '/docs/intro',
              },
            ],
          },
          {
            title: 'Mis redes',
            items: [
              {
                label: 'Correo electronico',
                href: 'mailto:davidrg@davidrgfoss.com',
              },
              {
                label: 'GitLab',
                href: 'https://gitlab.com/davidrg',
              },
              {
              	label: 'GitHub',
              	href: 'https://github.com/davidrg28',
              },
            ],
          },
          {
            title: 'Mas',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Davidrg FOSS, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
